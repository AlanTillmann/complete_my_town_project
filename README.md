# MyTown Issue Board

## Project Description

The Issue Review board is an application for a small local town. The application allows constituents to submit issues that they have noticed in the town. The review board members can look over these issues. Board members can then create meetings which can be viewed by constituents and display information on what needs to be discussed. The application is a microservices architecture that is entirely container based. 

## Technologies Used

* Google Pub/Sub
* Google Kubernetes Engine
* Google Cloud Run
* Google Container Registry
* Datastore
* MaterialUI
* Docker

## Features

* Can schedule meetings
* View all current meetings and issues posted
* Filter issues based on their properties

## Getting Started

1. git clone the repository.
2. Run "npm install" to get all dependencies if running the program locally.
    - Use "npm start" to start each backend service and the frontend.
3. Deploy all services to GCP and host frontend on Firebase if not running program locally.
4. To have full access to application functionality, you must login with the password (the edit or delete a meeting).
    - If you do not have the password (meant for "council members"), then you can only GET meetings.

## Usage

* The frontend is on a single page, so there is no navigation required.
* A password is required for authentication to access certain features (as they are for council members only).
* You can submit an issue without authentication as that feature is meant for towns people.
* You can filter and see all of the meetings and issues regardless of if you are a towns person or council member.

## Contributors

* Jarrick Hillery
* James Kirk
* Camden Snyder
* Alan Tillmann
